#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>

using namespace std;
/// <summary>
/// Klasa reprezentujaca punkt, oraz jego metody.
/// </summary>
class Point {
	public:
		/// <summary>Wspolrzedna x.</summary>
		float x;
		/// <summary>Wspolrzedna y.</summary>
		float y;
		
		/// <summary>
		/// Konstruktor punktu.
		/// </summary>	
		/// <param name="x">Wspolrz�dna x.</param>
		/// <param name="y">Wspolrz�dna y.</param>
		Point(float x, float y) {
			this->x = x;
			this->y = y;
		}
		/// <summary>
		/// Metoda sluzaca do przesuniecia punktu.
		/// </summary>	
		/// <param name="x">Odleg�osc na osi x.</param>
		/// <param name="y">Odleg�osc na osi y.</param>
		void move(float x, float y) {
			this->x += x;
			this->y += y;
		}
};


/// <summary>
/// Klasa reprezentujaca tabele punktow i promieni figur.
/// </summary>
class Vectors{
	public:
	/// <summary>Tabela przechowujaca srodki figur w postaci punktow.</summary>
	vector<Point> centers;
	/// <summary>Tabela przechowuj�ca dlugosci promieni figur.</summary>
	vector<int> radiuses;

}Tables;

	

/// <summary>
/// Klasa reprezentujaca figure.
/// </summary>
class Figure {

		
	public:
		/// <summary>Tabela zawieraj�ca punkty o wspolrzednych wierzcholkow figury.</summary>
		vector<Point> corners;

		/// <summary>
		/// Metoda wypisujaca wspolrzedne wszystkich wierzcholkow.
		/// </summary>	
		void printCornersCoordinates() {
			for(int i = 0; i < corners.size(); i++) {
				Point corner = corners.at(i);
				
				cout << "[" << corner.x << ", " << corner.y << "]" << endl;
			}
		}
		
		
		/// <summary>
		/// Metoda przypisujaca losowa liczbe z zakresu [500...1499] do wspolrzednej x.
		/// </summary>
		/// <returns>Zwraca wartosc wspolrzednej x.</returns>
		int getRandomX(){
			int x;
			x = rand()%1000+500;
			return x;
		}
		
		
		/// <summary>
		/// Metoda przypisujaca losowa liczbe z zakresu [500...1499] do wspolrzednej y.
		/// </summary>
		/// <returns>Zwraca wartosc wspolrzednej y.</returns>
		int getRandomY(){
			int y;
			y = rand()%1000+500;
			return y;
		}
		
		/// <summary>
		/// Metoda przypisujaca losowa liczbe z zakresu [1...500] do dlugosci promienia.
		/// </summary>
		/// <returns>Zwraca dlugosc promienia.</returns>
		int getRandomRadius(){
			int radius;
			radius = rand()%500+1;
			return radius;
		}

		
};

/// <summary>
/// Klasa reprezentujaca wielokat foremny.
/// </summary>
class Polygon: public Figure{
	/// <summary>Wspolrzedna x srodka wielokata.</summary>
	int center_x;
	/// <summary>Wspolrzedna y srodka wielokata.</summary>
	int center_y;
	/// <summary>Dlugosc promienia kola opisanego na wielokacie.</summary>
	int radius;
	/// <summary>Ilosc wierzcholkow.</summary>
	float cornersCount;
	/// <summary>Wspolrzedna x wierzcholka.</summary>
	float corner_x;
	/// <summary>Wspolrzedna y wierzcholka.</summary>
	float corner_y;
	
	public:
		/// <summary>
		/// Konstruktor generujacy wielokat o zadanej ilosci wierzcholkow.
		/// </summary>
		/// <param name="cornersCount">Ilosc wierzcholkow.</param>
		Polygon(int cornersCount){
			bool canCreate;
			int counter = 0;
			
			
			this->cornersCount = cornersCount;
			float alpha = 2*M_PI/(float)cornersCount;
			
			do{
				counter++;
				canCreate = true;
				this->center_x = getRandomX();
				this->center_y = getRandomY();
				this->radius = getRandomRadius();
				

				
				
					for(int i=0;  i<Tables.centers.size(); i++){
						float centersDistance;
						float radiusDistance;
						centersDistance = sqrt( pow(Tables.centers.at(i).x - center_x,2 ) + pow( Tables.centers.at(i).y - center_y,2 ));
						radiusDistance = (float)Tables.radiuses.at(i) + (float)radius;
						
						if(centersDistance <= radiusDistance){
							canCreate = false;
							break;
						}
						
					}
				
				
			}
			while(canCreate == false);
			
			cout << "Wygenerowano " << counter << " " << cornersCount << "-katow"<< endl;
			Tables.centers.push_back(Point(center_x,center_y));
			Tables.radiuses.push_back(radius);
			
			
			
			for(int i=0; i<cornersCount; i++){
				corner_x = center_x+radius*sin(i*alpha);
				corner_y = center_y+radius*sin(i*alpha);
				
				this->corners.push_back(Point(corner_x,corner_y));
			}

		}
		
};	
/// <summary>
/// Klasa reprezentujaca kolo.
/// </summary>
class Circle: public Figure{
	/// <summary>Wspolrzedna x srodka wielokata.</summary>
	int center_x;
	/// <summary>Wspolrzedna y srodka wielokata.</summary>
	int center_y;
	/// <summary>Dlugosc promienia kola opisanego na wielokacie.</summary>
	int radius;
	public:
		/// <summary>
		/// Konstruktor generujacy kolo.
		/// </summary>
		Circle(){
			bool canCreate;
			int counter = 0;
			
			
			
			
			do{
				counter++;
				canCreate = true;
				this->center_x = getRandomX();
				this->center_y = getRandomY();
				this->radius = getRandomRadius();
				

				
				
					for(int i=0;  i<Tables.centers.size(); i++){
						float centersDistance;
						float radiusDistance;
						centersDistance = sqrt( pow(Tables.centers.at(i).x - center_x,2 ) + pow( Tables.centers.at(i).y - center_y,2 ));
						radiusDistance = (float)Tables.radiuses.at(i) + (float)radius;
						
						if(centersDistance <= radiusDistance){
							canCreate = false;
							break;
						}
						
					}
				
				
			}
			while(canCreate == false);
			
			cout << "Wygenerowano " << counter << " kol" << endl;
			Tables.centers.push_back(Point(center_x,center_y));
			Tables.radiuses.push_back(radius);
			
			this->corners.push_back(Point(center_x,center_y));
		}
	
	
};

int main() {
	srand(time(NULL));
	
	vector<Polygon> polygons;
	vector<Circle> circles;
	
	Circle c;
	for(int i=3; i<=9; i++){
		polygons.push_back(Polygon(i));
	}
	
	
	cout << endl << "Kolo" << endl << "Srodek: ";
	c.printCornersCoordinates();
	cout << "Dlugosc promienia: " << Tables.radiuses.at(0) << endl;
	
	
	
	cout << endl;
	for(int n=3;n<10;n++){
    	cout<<n<<"-kat"<<endl;
		Polygon p = polygons.at(n-3);
		p.printCornersCoordinates();
		cout << endl;
    }
	
	system("PAUSE");
	return 0;
}

