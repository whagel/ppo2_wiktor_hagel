<?php

use App\Controllers\Controller;
use App\Helpers\Result;
use PHPUnit\Framework\TestCase;

final class ApplicationTest extends TestCase {

	public function testIfTrueIsTrue(): void {
		$this->assertTrue(true);
	}

	public function testIfEmptyRequestIsHandledProperly(): void {
		$result = $this->getResultFromController([]);
		$this->assertNull($result);
	}

	public function testIfInvalidRequestIsHandledProperly(): void {
		$result = $this->getResultFromController(["test" => "test"]);
		$this->assertNull($result);
	}

    public function testIfEmptyActionIsHandledProperly(): void {
        $result = $this->getResultFromController(["action" => ""]);
        $this->assertNull($result);
    }

    public function testIfWrongActionIsHandledProperly(): void {
        $result = $this->getResultFromController(["action" => "WRONG"]);
        $this->assertNull($result);
    }

	public function testIfQuoteActionIsHandledProperly(): void {
		$result = $this->getResultFromController(["action" => "quote"]);
		$this->assertNotNull($result);
		$this->assertInstanceOf(Result::class, $result);
		$this->assertContains($result->content, Controller::QUOTES);
	}


    public function testIfRecipeActionIsHandledProperly(): void {
        $result = $this->getResultFromController(["action" => "recipe"]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Result::class, $result);

        $recipes = [
            "Składniki: <ul><li>chleb tostowy</li><li>szynka</li><li>ser</li></ul> Przepis: <ul><li>położyć ser i szynkę na kromkę chleba</li><li>przykryć to kromką chleba</li><li>opiec w opiekaczu</li></ul>",
            "Składniki: <ul><li>3 jajka</li><li>3 plastry boczku</li><li>sól</li><li>pieprz</li></ul> Przepis: <ul><li>usmażyć boczek</li><li>wbić nań jajka</li><li>smażyć aż będzie wyglądało apetycznie</li><li>przyprawić</li></ul>",
        ];

        $this->assertContains($result->content, $recipes);
    }

    public function testIfTriviaActionIsHandledProperly(): void {
        $result = $this->getResultFromController(["action" => "trivia"]);
        $this->assertNotNull($result);
        $this->assertInstanceOf(Result::class, $result);

        $trivia = [
            "Mango to narodowy owoc Indii, Pakistanu i Filipin.",
            "Konfederacja Tamoios to pierwszy poważny bunt brazylijskich Indian przeciwko Europejczykom.",
            "Ostatnia w Polsce i jedna z ostatnich w Europie epidemia ospy prawdziwej wybuchła latem 1963 roku we Wrocławiu."
        ];

        $this->assertContains($result->content, $trivia);
    }

    public function testIfResultDataIsString(): void {
        $result = $this->getResultFromController(["action" => "quote"]);
        $this->assertInternalType("string", $result->title);
        $this->assertInternalType("string", $result->content);
        $result = $this->getResultFromController(["action" => "recipe"]);
        $this->assertInternalType("string", $result->title);
        $this->assertInternalType("string", $result->content);
        $result = $this->getResultFromController(["action" => "trivia"]);
        $this->assertInternalType("string", $result->title);
        $this->assertInternalType("string", $result->content);
    }

    public function testIfResultTitleIsNotEmpty(): void {
        $result = $this->getResultFromController(["action" => "quote"]);
        $this->assertNotEmpty($result->title);
        $result = $this->getResultFromController(["action" => "recipe"]);
        $this->assertNotEmpty($result->title);
        $result = $this->getResultFromController(["action" => "trivia"]);
        $this->assertNotEmpty($result->title);
    }

    public function testIfResultContentIsNotEmpty(): void {
        $result = $this->getResultFromController(["action" => "quote"]);
        $this->assertNotEmpty($result->content);
        $result = $this->getResultFromController(["action" => "recipe"]);
        $this->assertNotEmpty($result->content);
        $result = $this->getResultFromController(["action" => "trivia"]);
        $this->assertNotEmpty($result->content);
    }



	protected function getResultFromController(array $request): ?Result {
		$controller = new Controller();
		return $controller->getResult($request);
	}

}
